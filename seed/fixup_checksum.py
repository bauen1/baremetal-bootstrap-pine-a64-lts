#!/usr/bin/env python3
# Usage: ./fixup_checksum.py < boot0.bin > boot0_fixed.bin

import io, struct, sys

data = sys.stdin.buffer.read()

(bi, magic, checksum, size) = struct.unpack_from('<4s8sii', data, offset=0)

assert magic == b'eGON.BT0'
assert checksum == 0x5F0A6C39
assert size & 0x3 == 0
assert len(data) == size

checksum_calc = 0

i = 0
while i < len(data):
    (v,) = struct.unpack_from('<I', data, offset=i)
    checksum_calc += v
    # sys.stderr.write(f"v: {v}\n")
    i += 4

bytes_buf = bytearray(data)
struct.pack_into('<I', bytes_buf, 12, checksum_calc & 0xFFFFFFFF)

sys.stdout.buffer.write(bytes_buf)
