# Binary seed

This is the initial binary blob that is written to the SPI Flash.

It is short enough to be assembled by hand, and can in theory be written to an SPI Flash chip by hand using a small logic circuit.

It provides:

1. A tiny UART driver
2. A hex0 monitor
3. Some init code to setup clocks and UART0 and enter the hex0 monitor
4. A header to make it loadable from SPI Flash by the BROM

## Build

```bash
arm-none-eabi-as boot0.S -o boot0.o
arm-none-eabi-ld -nostdlib --entry=0x0 -Ttext=0x0 boot0.o -o boot0.elf
arm-none-eabi-objcopy -Obinary boot0.{elf,bin}
arm-none-eabi-objdump -xard boot0.elf
./fixup_checksum.py < boot0.bin > boot0_fixed.bin && /tools/sunxi-fel spiflash-write 0x0 ./boot0_fixed.bin
```

Copyright (C) 2021-2022 Jonathan Hettwer (bauen1)
